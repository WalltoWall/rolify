require 'rolify/railtie' if defined?(Rails)
require 'rolify/utils'
require 'rolify/role'
require 'rolify/configure'
require 'rolify/dynamic'
require 'rolify/resource'
require 'rolify/adapters/base'

module Rolify
  extend Configure

  attr_accessor :role_cname, :adapter, :join_table

  def rolify( options = { } )
    include Role
    extend Dynamic if Rolify.dynamic_shortcuts

    @options = set_options( options )
    
    rolify_options = { :class_name => @options[:class_name] }
    rolify_options.merge!({ :join_table => @options[:join_table] }) if Rolify.orm == "active_record"
    has_and_belongs_to_many :roles, rolify_options

    self.adapter = Rolify::Adapter::Base.create("role_adapter", @options[:class_name], self.name)
    self.role_cname = @options[:class_name]
    
    load_dynamic_methods if Rolify.dynamic_shortcuts
  end

  def resourcify(options = { })
    include Resource
    @options = set_options( options )
    
    resourcify_options = { :class_name => @options[:class_name].camelize, :as => :resource }
    has_many :roles, resourcify_options
    
    self.adapter = Rolify::Adapter::Base.create("resource_adapter", @options[:class_name], self.name)
    self.role_cname = @options[:class_name]
  end
  
  def scopify
    require "rolify/adapters/#{Rolify.orm}/scopes.rb"
  end
  
  def role_class
    self.role_cname.constantize
  end

  private
  def set_options(opts)
    @options = {
        class_name: 'Role',
        join_table: "#{self.to_s.tableize}_roles"
    }.merge(opts)
    @options
  end
end